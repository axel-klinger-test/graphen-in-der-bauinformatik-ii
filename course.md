# Graphen in der Bauinformatik

* Ein paar Grundlagen 
* Suchalgorithmen
* Praktisches Beispiel mit einem kleinen Editor

Ein Graph (selten auch Graf[1]) ist in der Graphentheorie eine abstrakte Struktur, die eine Menge von Objekten zusammen mit den zwischen diesen Objekten bestehenden Verbindungen repräsentiert. Die mathematischen Abstraktionen der Objekte werden dabei Knoten (auch Ecken) des Graphen genannt. Die paarweisen Verbindungen zwischen Knoten heißen Kanten (manchmal auch Bögen). Die Kanten können gerichtet oder ungerichtet sein. Häufig werden Graphen anschaulich gezeichnet, indem die Knoten durch Punkte und die Kanten durch Linien dargestellt werden.[2]
[Wikipedia](https://de.wikipedia.org/wiki/Graph_(Graphentheorie))

![Screenshot eines Editors](images/screen.png)

![Ein Beispiel Graph](https://upload.wikimedia.org/wikipedia/commons/0/0c/Dijkstra-negative-edge-weights-error.svg)

![Ein Beispiel Graph](https://upload.wikimedia.org/wikipedia/commons/7/7f/DijkstraStep03.svg)


## Ein kleiner Editor für Graphen

[Ein Editor auf GitHub](https://github.com/axel-klinger/grapheneditor)
